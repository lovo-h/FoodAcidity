// TODO: add unit-tests

import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApolloQueryResult } from 'apollo-client';

import { NutdataModel, NutDataResponse } from '../models/nutdata.model';
import { SnippetModel, SnippetResponse } from '../models/snippet.model';


const LongDescsGQL = gql`
    query LongDescsGQL($keywords: [String!]!) {
      longDescs(keywords: $keywords) {
        ndbNo longDesc fdGrpDesc fdGrpCd
      }
    }
`;

const NutrientsGQL = gql`
    query NutrientsGQL($ndbno: String!) {
      nutrients(ndbNo: $ndbno) {
        nutrNo nutrDesc nutrVal units
      }
    }
`;


@Injectable({
  providedIn: 'root'
})
export class FoodGQL {
  constructor(private apollo: Apollo) {
  }

  getSnippets(snippet: String): Observable<SnippetModel[]> {
    const re = /[^a-zA-Z0-9 ]/g;
    const snips = snippet.replace(re, '').split(' ');

    return this.apollo.watchQuery<SnippetResponse>({
      query: LongDescsGQL,
      variables: {
        keywords: snips
      }
    }).valueChanges.pipe(map((data) => {
      const snippetDataArr: SnippetModel[] = [];

      for (let idx = 0; idx < data.data.longDescs.length; idx++) {
        snippetDataArr.push(new SnippetModel(data.data.longDescs[idx]));
      }

      return snippetDataArr;
    }));
  }

  getNutritionDataForNdbNo(ndbNo: String): Observable<NutdataModel> {
    return this.apollo.watchQuery<NutDataResponse>({
      query: NutrientsGQL,
      variables: {
        ndbno: ndbNo
      }
    }).valueChanges.pipe(map((data: ApolloQueryResult<NutDataResponse>) => {
      if (data.data.nutrients.length < 5) {
        throw new Error('Error: missing nutrition data');
      }

      return new NutdataModel(data.data.nutrients);
    }));
  }
}
