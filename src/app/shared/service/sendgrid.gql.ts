import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApolloQueryResult } from 'apollo-client';


const AddRecipientGQL = gql`
    mutation AddRecipientGQL($firstName: String!, $lastName: String!, $email: String!) {
      addRecipient(firstName: $firstName, lastName: $lastName, email: $email)
    }
`;

const EmailUserEdibleListGQL = gql`
    mutation EmailUserEdibleListGQL($edibleFoods: [String!]!, $email: String!) {
      emailUserEdibleList(edibleFoods: $edibleFoods, email: $email)
    }
`;

interface EmailUserEdibleListGQLResponse {
  emailUserEdibleList: boolean;
}

interface AddRecipientGQLResponse {
  addRecipient: boolean;
}


@Injectable({
  providedIn: 'root'
})
export class SendGridGQL {
  constructor(private apollo: Apollo) {
  }

  emailEdibleFoodList(email: string, edibleFoodList: string[]): Observable<Boolean> {
    return this.apollo.mutate<EmailUserEdibleListGQLResponse>({
      mutation: EmailUserEdibleListGQL,
      variables: {
        edibleFoods: edibleFoodList,
        email: email
      }
    }).pipe(map((data: ApolloQueryResult<EmailUserEdibleListGQLResponse>) => {
      return data.data.emailUserEdibleList;
    }));
  }

  subscribeWithUserInfo(firstName: string, lastName: string, email: string): Observable<Boolean> {
    return this.apollo.mutate<AddRecipientGQLResponse>({
      mutation: AddRecipientGQL,
      variables: {
        firstName: firstName,
        lastName: lastName,
        email: email
      }
    }).pipe(map((data: ApolloQueryResult<AddRecipientGQLResponse>) => {
      return data.data.addRecipient;
    }));
  }

}
