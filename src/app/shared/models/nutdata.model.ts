export interface NutDataResponse {
  nutrients: NutrDataModelHelper[];
}

export interface NutrDataModelHelper {
  nutrNo: string;
  nutrDesc: string;
  nutrVal: string;
  units: string;
}

export class NutVal {
  val: number;
  unit: string;

  constructor(val: number, unit: string) {
    this.val = val;
    this.unit = unit;
  }
}

export class NutdataModel {
  protein: NutVal;
  calcium: NutVal;
  magnesium: NutVal;
  phosphorus: NutVal;
  potassium: NutVal;

  constructor(nutrDataArr: NutrDataModelHelper[]) {
    let nutrVal: NutVal;

    for (let idx = 0; idx < nutrDataArr.length; idx++) {
      nutrVal = new NutVal(Number(nutrDataArr[idx].nutrVal), nutrDataArr[idx].units);

      switch (nutrDataArr[idx].nutrNo) {
        case '203':
          this.protein = nutrVal;
          break;
        case '301':
          this.calcium = nutrVal;
          break;
        case '304':
          this.magnesium = nutrVal;
          break;
        case '305':
          this.phosphorus = nutrVal;
          break;
        case '306':
          this.potassium = nutrVal;
          break;
        default:
      }
    }
  }

}
