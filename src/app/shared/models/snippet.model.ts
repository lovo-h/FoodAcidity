export interface SnippetResponse {
  longDescs: SnippetModelHelper[];
}

export interface SnippetModelHelper {
  fdGrpCd: string;
  fdGrpDesc: string;
  longDesc: string;
  ndbNo: string;
}

export class SnippetModel {
  fdGrpCd: string;
  fdGrpDesc: string;
  longDesc: string;
  ndbNo: string;

  constructor(modelHelper: SnippetModelHelper) {
    this.ndbNo = modelHelper.ndbNo;
    this.longDesc = modelHelper.longDesc;
    this.fdGrpDesc = modelHelper.fdGrpDesc;
    this.fdGrpCd = modelHelper.fdGrpCd;
  }
}
