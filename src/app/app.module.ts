import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { MealValidatorModule } from './meal-validator/meal-validator.module';

import {
  AppRoutingModule,
  FooterComponent,
  SharedModule
} from './shared';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
  ],
  imports: [
    ApolloModule,
    AppRoutingModule,
    BrowserModule,
    HomeModule,
    HttpLinkModule,
    MealValidatorModule,
    SharedModule
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory(httpLink: HttpLink) {
        return {
          cache: new InMemoryCache(),
          link: httpLink.create({
            uri: '/api/query'
          })
        };
      },
      deps: [HttpLink]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
