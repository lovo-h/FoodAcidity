# FoodAcidity

---
[![CircleCI](https://circleci.com/gh/lovo-h/FoodAcidity/tree/master.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/lovo-h/FoodAcidity/tree/master)
--- 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
